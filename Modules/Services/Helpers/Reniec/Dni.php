<?php

namespace Modules\Services\Helpers\Reniec;

use Modules\Services\Helpers\HttpConnectionApi;


class Dni
{

    public const BASE_URL = 'https://ww1.sunat.gob.pe/ol-ti-itatencionf5030/registro/solicitante?tipDocu=1&tipPers=1&numDocu=';
        
    /**
     *
     * @param  string $number
     * @return array
     */
    public static function search($number)
    {
        $validate = self::validateData($number);
        if(!$validate['success']) return $validate;

        $url = self::BASE_URL.$number;

        $response = (new HttpConnectionApi())->sendRequest($url, 'POST');

        return self::parseResponse($response, $number);
    }


    /**
     * 
     * Validaciones
     *
     * @param  string $number
     * @return array
     */
    private static function validateData($number)
    {
        if(!is_numeric($number)) return self::getResponseArray(false, 'El número de DNI no es válido.');
        
        if(strlen($number) != 8) return self::getResponseArray(false, 'El número de DNI no tiene 8 dígitos.');

        return self::getResponseArray(true, null);
    }

    
    /**
     * 
     * Procesar respuesta
     *
     * @param  array $response
     * @param  string $number
     * @return array
     */
    private static function parseResponse($response, $number)
    {
        $maternal_surname = $response['apeMatSoli'] ?? null;
        $paternal_surname = $response['apePatSoli'] ?? null;
        $name = $response['nombreSoli'] ?? null;

        if($maternal_surname && $paternal_surname && $name)
        {
            $data = [
                'numero' => $number,
                'nombre_completo' => "{$paternal_surname} {$maternal_surname}, {$name}",
                'nombres' => $name,
                'apellido_paterno' => $paternal_surname,
                'apellido_materno' => $maternal_surname,
                'ubigeo_sunat' => null,
                'ubigeo' => [null, null, null]
            ];

            return self::getResponseArray(true, 'Número de DNI encontrado.', $data);
        }

        return self::getResponseArray(false, 'El número de DNI no fue encontrado.');
    }
    

    /**
     *
     * @param  bool $success
     * @param  string $message
     * @return array
     */
    private static function getResponseArray($success, $message, $data = [])
    {
        $response = [
            'success' => $success,
            'message' => $message
        ];

        if(!empty($data)) $response['data'] = $data;

        return $response;
    }

}