<?php

namespace Modules\Services\Helpers\Sunat;

use Modules\Services\Helpers\HttpConnectionApi;
use App\Models\Empresa;
use Modules\Services\Models\District;


class Ruc
{
    
    /**
     * 
     * Buscar RUC
     *
     * @param  string $number
     * @return array
     */
    public static function search($number)
    {
        $validate = self::validateData($number);
        if(!$validate['success']) return $validate;

        $company = Empresa::where('ruc', $number)->first();

        return self::parseResponse($company);
    }

    
    /**
     * 
     * Validaciones
     *
     * @param  string $number
     * @return array
     */
    private static function validateData($number)
    {
        if(!is_numeric($number)) return self::getResponseArray(false, 'El número de RUC no es válido.');
        
        if(strlen($number) != 11) return self::getResponseArray(false, 'El número de RUC no tiene 11 dígitos.');

        return self::getResponseArray(true, null);
    }

    
    /**
     * 
     * Procesar respuesta
     *
     * @param  Empresa $company
     * @return array
     */
    private static function parseResponse($company)
    {
        if($company)
        {
            $address = self::getAddress($company);
            $location = self::getDataLocation($company->ubigeo, $address);

            $data = [
                'direccion' => $location['address'],
                'direccion_completa' => $location['address_full'],
                'ruc' => $company->ruc,
                'nombre_o_razon_social' => $company->nombre_razon_social,
                'estado' => $company->estado_contribuyente,
                'condicion' => $company->condicion_domicilio,
                'departamento' => $location['departamento'],
                'provincia' => $location['provincia'],
                'distrito' => $location['distrito'],
                'ubigeo_sunat' => $company->ubigeo,
                'ubigeo' => $location['ubigeo'],
                'es_agente_de_retencion' => null,
                'anexos' => []
            ];

            return self::getResponseArray(true, 'Número de RUC encontrado.', $data);
        }

        return self::getResponseArray(false, 'El número de RUC no fue encontrado.');
    }
    

    /**
     *
     * @param  bool $success
     * @param  string $message
     * @return array
     */
    private static function getResponseArray($success, $message, $data = [])
    {
        $response = [
            'success' => $success,
            'message' => $message
        ];

        if(!empty($data)) $response['data'] = $data;

        return $response;
    }

    
    /**
     * 
     * Dirección completa
     *
     * @param  Empresa $company
     * @return string
     */
    private static function getAddress($company)
    {
        $tipo_via = ($company->tipo_via && $company->tipo_via != '-') ? $company->tipo_via : '';
        $nombre_via = ($company->nombre_via && $company->nombre_via != '-') ? ' '.$company->nombre_via : '';
        $codigo_zona = ($company->codigo_zona && $company->codigo_zona != '-') ? ' '.$company->codigo_zona : '';
        $tipo_zona = ($company->tipo_zona && $company->tipo_zona != '-') ? ' '.$company->tipo_zona : '';
        $numero = ($company->numero && $company->numero != '-') ? " NRO. {$company->numero}" : '';
 
        $manzana = ($company->manzana && $company->manzana != '-') ? " MZ. {$company->manzana}" : '';
        $lote = ($company->lote && $company->lote != '-') ? " LT. {$company->lote}" : '';
        $departamento = ($company->departamento && $company->departamento != '-') ? " DPTO. {$company->departamento}" : '';
        $interior = ($company->interior && $company->interior != '-') ? " INT. {$company->interior}" : '';
        $kilometro = ($company->kilometro && $company->kilometro != '-') ? " KM. {$company->kilometro}" : '';

        $address = "{$tipo_via}{$nombre_via}{$numero}{$codigo_zona}{$tipo_zona}{$manzana}{$lote}{$departamento}{$interior}{$kilometro}";

        return $address;
    }
    

    /**
     *
     * @param  int $location_id
     * @param  string $address
     * @return array
     */
    private static function getDataLocation($location_id, $address)
    {
        $district = District::with('province')->find($location_id);

        if(is_null($district)) 
        {
            return [
                'ubigeo' => [],
                'address' => $address,
                'address_full' => $address,
            ];
        }

        $department_name = mb_strtoupper($district->province->department->description);
        $province_name = mb_strtoupper($district->province->description);
        $district_name = mb_strtoupper($district->description);
        $location_full_name = $department_name.' - '.$province_name.' - '.$district_name;

        $ubigeo = [
            $district->province->department_id,
            $district->province_id,
            $district->id,
        ];
        
        return [
            'ubigeo' => $ubigeo,
            'address' => $address,
            'address_full' => $address.', '.$location_full_name,
            'departamento' => $department_name,
            'provincia' => $province_name,
            'distrito' => $district_name,
        ];
    }

}