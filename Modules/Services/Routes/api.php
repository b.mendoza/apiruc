<?php
 
Route::middleware('auth:api')->group(function () {
   
    // Route::post('validar-cpe', 'ServiceController@documentValidate');
    Route::get('consulta/dni/{dni}', 'ServiceController@dni');
    Route::get('consulta/ruc/{ruc}', 'ServiceController@ruc');
          
});