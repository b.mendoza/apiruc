<?php

namespace App\Models;


class ChargePadron extends BaseModel
{
    protected $table = 'charges_data_padron';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
        
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];
}