<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::group(['middleware' => 'auth'], function() {

    Route::get('user', 'HomeController@user')->name('user_index');
    Route::get('padron_index', 'HomeController@padron')->name('padron_index');
    Route::get('padron/download', 'PadronController@download');
    Route::get('padron/extractor', 'PadronController@extractor');
    Route::get('padron/loadtdata', 'PadronController@loadtdata');

});
